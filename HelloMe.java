import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

//Case 1: Create MyActionListener Class for addMyActionListener

class MyActionListener implements ActionListener{
    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("MyActionListener : Action");
    }
}

public class HelloMe implements ActionListener {
    public static void main(String[] args) {
        JFrame frmMain = new JFrame("Hello Me");
        frmMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmMain.setSize(600, 400);

        JLabel lblYourName = new JLabel("Your Name : ");
        lblYourName.setSize(80,20);
        lblYourName.setLocation(5,5);
        lblYourName.setBackground(Color.LIGHT_GRAY);
        lblYourName.setOpaque(true);

        JTextField txtYourName = new JTextField();
        txtYourName.setSize(200, 20);
        txtYourName.setLocation(90,5);

        JButton btnHello = new JButton("Hello");
        btnHello.setSize(80, 20);
        btnHello.setLocation(140, 50);

        //Case 1: Create MyActionListener Class for addMyActionListener

        MyActionListener myActionListener = new MyActionListener();
        btnHello.addActionListener(myActionListener);

        //Case 2: Fix HelloMe Class to implements ActionListener for addActionListener
        btnHello.addActionListener(new HelloMe());

        // Ex. Create Anonymous Class 
        ActionListener actionListener = new ActionListener(){ 
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Anonymous Class : Action");
            }
        };
        btnHello.addActionListener(actionListener);

        JLabel lblHello = new JLabel("Hello ...", JLabel.CENTER);
        lblHello.setSize(200, 20);
        lblHello.setLocation(140, 100);
        lblHello.setBackground(Color.LIGHT_GRAY);
        lblHello.setOpaque(true);

        frmMain.setLayout(null);

        frmMain.add(lblYourName);
        frmMain.add(txtYourName);
        frmMain.add(btnHello);
        frmMain.add(lblHello);

        btnHello.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = txtYourName.getText();
                lblHello.setText("Hello " + name);
            }
        });

        frmMain.setVisible(true);
    }

    //Case 2: Fix HelloMe Class to implements ActionListener for addActionListener
    @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println("HelloMe : Action");
        }
}
