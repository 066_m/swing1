//import javax.swing.*;
//import java.awt.*;
import javax.swing.JFrame;
import java.awt.Dimension;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Font;

public class Frame1 {
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(600, 400);
        //frame.setSize(new Dimension(600,400)); --- Overloading method

        JLabel lblHelloWorld = new JLabel("Hello World", JLabel.CENTER);
        lblHelloWorld.setBackground(Color.cyan);
        lblHelloWorld.setOpaque(true);
        lblHelloWorld.setFont(new Font("Verdana", Font.BOLD, 25));
        frame.add(lblHelloWorld);

        frame.setVisible(true);
    }
}
